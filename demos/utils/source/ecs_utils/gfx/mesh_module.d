module ecs_utils.gfx.mesh_module;

import ecs_utils.gfx.material;
import ecs_utils.gfx.texture;
import ecs_utils.gfx.mesh;

struct MeshModule
{
    Mesh* mesh;
    Material* material;
    Texture texture;
}