module ecs_utils.gfx.sprite;

import ecs_utils.math.matrix;
import ecs_utils.gfx.mesh_module;

struct sprite
{
    MeshModule* mesh;

    mat3 matrix;
}