module ecs_utils.gfx.render_list;

import ecs_utils.gfx.mesh_module;
import ecs_utils.math.vector;
import ecs_utils.math.matrix;
import ecs_utils.gfx.config;

struct RenderList
{
    struct Data
    {
        MeshModule* module_;
        uint index;
    }

    struct LocScale
    {
        vec2 location;
        vec2 scale;
    }

    struct Layer
    {
        LayerType type;
        Data[] list;
        LocScale[] loc_scale;
        mat3[] matrices;
    }
}