/*

    OpenGL, OpenGL ES loader generated by glad 0.1.33 on Fri Nov  8 17:14:30 2019.

    Language/Generator: D
    Specification: gl
    APIs: gl=3.3, gles2=3.0
    Profile: compatibility
    Extensions:
        
    Loader: True
    Local files: False
    Omit khrplatform: False
    Reproducible: False

    Commandline:
        --profile="compatibility" --api="gl=3.3,gles2=3.0" --generator="d" --spec="gl" --extensions=""
    Online:
        https://glad.dav1d.de/#profile=compatibility&language=d&specification=gl&loader=on&api=gl%3D3.3&api=gles2%3D3.0
*/

module glad.gl.all;


public import glad.gl.funcs;
public import glad.gl.ext;
public import glad.gl.enums;
public import glad.gl.types;
