module gui.template_;

import bubel.ecs.entity;

struct TemplateGUI
{
    const (char)* name;
    EntityTemplate* tmpl;
}