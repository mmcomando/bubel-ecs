module gui.attributes;

enum GUIColor = "GUIColor";

struct GUIRange
{
    union 
    {
        struct
        {
            int min;
            int max;
        }
        struct
        {
            float minf;
            float maxf;
        }
    }
}