module demos.physics;

import app;

import bindbc.sdl;

import cimgui.cimgui;

import bubel.ecs.attributes;
import bubel.ecs.core;
import bubel.ecs.entity;
import bubel.ecs.manager;
import bubel.ecs.std;

import ecs_utils.gfx.texture;
import ecs_utils.math.vector;
import ecs_utils.utils;

extern(C):