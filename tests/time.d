module tests.time;


version (WebAssembly)
{
	alias int time_t;
	alias int clockid_t;
	enum CLOCK_REALTIME = 0;

	struct timespec
	{
		time_t tv_sec;
		int tv_nsec;
	}

	extern (C) int clock_gettime(clockid_t, timespec*) @nogc nothrow @system;

	struct Time
	{
		static long getUSecTime()
		{
			time_t time;
			timespec spec;

			clock_gettime(CLOCK_REALTIME, &spec);

			//time = spec.tv_sec;
			return spec.tv_sec * 1000_000 + spec.tv_nsec / 1000; //time / 1000_000;
		}
	}
}
else version (Windows)
{
	import core.stdc.stdio : printf;
	import core.sys.windows.windows;

	struct Time
	{
		static long getUSecTime()
		{
			LARGE_INTEGER time, freq;
			QueryPerformanceFrequency(&freq);
			QueryPerformanceCounter(&time);
			return time.QuadPart / (freq.QuadPart / 1000_000);
		}
	}
}
else version (Posix)
{
	import core.stdc.stdio : printf;
	import core.sys.posix.time;

	struct Time
	{
		static long getUSecTime()
		{
			time_t time;
			timespec spec;

			clock_gettime(CLOCK_REALTIME, &spec);

			//time = spec.tv_sec;
			return spec.tv_sec * 1000_000 + spec.tv_nsec / 1000; //time / 1000_000;
		}
	}
}