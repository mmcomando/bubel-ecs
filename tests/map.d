module tests.map;

import bubel.ecs.hash_map;

version(GNU)
{
	pragma(inline, true) T[n] staticArray(T, size_t n)(auto ref T[n] a)
	{
		return a;
	}
}
else import std.array : staticArray;

@("HashMap")
unittest
{
    HashMap!(string, int) map;

    assert(map.length == 0);
    map.add("asd",1);
    assert(map.length == 1);
    map.clear();
    assert(map.length == 0);
    map.add("asd",1);
    assert(map.length == 1);
    map.reset();
    assert(map.length == 0);

    map.add("asd",1);
    string asd = "asd";
    assert(map.isIn("asd"));
    assert(map.isIn(asd));
    assert(!map.isIn("asdf"));
    map.tryRemove("asdf");
    map.tryRemove("asd");
    assert(map.length == 0);
    map.add("asdf",1);
    map.add("asd",2);
    assert(map["asd"] == 2);
    assert(map["asdf"] == 1);
    assert(map.length == 2);
    map.tryRemove("asdf");
    assert(map.length == 1);
    map.remove("asd");
    assert(map.length == 0);

    map.add("asd",1);
    map.add("asdwwghe",6);
    foreach(k,v;&map.byKeyValue)
    {
        assert(map[k] == v);
    }
}