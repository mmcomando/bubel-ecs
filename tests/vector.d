module tests.vector;

import bubel.ecs.simple_vector;
import bubel.ecs.vector;

version(GNU)
{
	pragma(inline, true) T[n] staticArray(T, size_t n)(auto ref T[n] a)
	{
		return a;
	}
}
else import std.array : staticArray;

@("simple-vector")
unittest
{
    SimpleVector vector;
    vector.add(cast(ubyte[]) "a");
    vector.add(cast(ubyte[]) "bsdf");
    assert(vector[0 .. 5] == cast(ubyte[]) "absdf");
    assert(vector[4] == 'f');
    assert(vector[] == cast(ubyte[]) "absdf");
    assert(vector[$ - 1] == 'f');

    vector.clear();
    assert(vector.length == 0);

    ubyte[1025] array;
    foreach(i;0..cast(uint)array.length)array[i] = cast(ubyte)i;
    vector.add(array);
    assert(vector.length == 1025);
    assert(vector[] == array[]);

    SimpleVector vector2;
    vector2.clear();
    vector2.add(array[0..1023]);
    vector2.add('a');
    vector2.add('b');
    assert(vector2.length == 1025);
    assert(vector2[0..1023] == array[0..1023]);
    assert(vector2[1023] == 'a');
    assert(vector2[1024] == 'b');
}

@("Vector")
unittest
{
    struct G
    {
        int a;
    }

    Vector!G vector;
    assert(vector.empty());
    vector.add(G(1));
    assert(!vector.empty());
    vector.clear();
    assert(vector.empty());
    vector.add(G(1));
    assert(!vector.empty());
    vector.reset();
    assert(vector.empty());

    vector.add(G(1));
    vector.add([G(2),G(5)].staticArray);
    assert(vector.length == 3);
    assert(vector.capacity == 1);
    
    Vector!G vector2;
    vector2.add([G(1),G(2),G(5)].staticArray);
    assert(vector == vector2);
    vector2.remove(1);
    assert(vector != vector2);
    assert(vector2.length == 2);
    assert(vector2[1] == G(5));
    vector2.add(G(2),1);
    assert(vector == vector2);
}
