module tests.bugs;

import tests.basic;

import bubel.ecs.core;
import bubel.ecs.manager;
import bubel.ecs.system;
import bubel.ecs.attributes;

version(GNU)
{
	pragma(inline, true) T[n] staticArray(T, size_t n)(auto ref T[n] a)
	{
		return a;
	}
}
else import std.array : staticArray;

@("Bug0001")
unittest
{
    struct Event1
    {
        mixin ECS.Event;

        EntityID id;
    }

    struct Event2
    {
        mixin ECS.Event;
    }

    struct System1
    {
        mixin ECS.System;

        struct EntitiesData
        {
            CInt[] int_;
        }

        EntityTemplate* tmpl;
        EntityID id;

        void onCreate()
        {
            tmpl = gEM.allocateTemplate([CInt.component_id, CLong.component_id].staticArray);
        }

        void onDestroy()
        {
            gEM.freeTemplate(tmpl);
        }

        void handleEvent(Entity* entity, Event1 event)
        {
            gEM.removeEntity(event.id);
            gEM.sendEvent(entity.id,Event2());
        }

        void handleEvent(Entity* entity, Event2 event)
        {
            id = gEM.addEntity(tmpl).id;
        }
    }

    struct System2
    {
        mixin ECS.System;

        struct EntitiesData
        {
            Entity[] entity;
        }

        ///check if every entity was removed correctly
        void onUpdate(EntitiesData data)
        {
            assert(0);
        }
    }

    struct System3
    {
        mixin ECS.System;

        struct EntitiesData
        {
            uint length;
            Entity[] entity;
        }

        ///remove every entity
        void onUpdate(EntitiesData data)
        {
            foreach(i;0..data.length)gEM.removeEntity(data.entity[i].id);
        }
    }

    gEM.initialize(0);

    gEM.beginRegister();

    gEM.registerComponent!CInt;
    gEM.registerComponent!CFloat;
    gEM.registerComponent!CDouble;
    gEM.registerComponent!CLong;
    gEM.registerComponent!CShort;
    gEM.registerComponent!CFlag;

    gEM.registerEvent!Event1;
    gEM.registerEvent!Event2;

    gEM.registerSystem!System1(0);
    gEM.registerSystem!System2(-200);
    gEM.registerSystem!System3(-200);

    gEM.endRegister();

    EntityTemplate* tmpl = gEM.allocateTemplate([CInt.component_id, CLong.component_id].staticArray);
    EntityID id = gEM.addEntity(tmpl,[CLong(10).ref_, CInt(6).ref_].staticArray).id;
    EntityID id2 = gEM.addEntity(tmpl,[CInt(4).ref_].staticArray).id;
    gEM.freeTemplate(tmpl);
    gEM.commit();

    gEM.sendEvent(id2, Event1(id));

    gEM.getSystem(System2.system_id).disable();

    gEM.begin();
    gEM.update();
    gEM.end();

    gEM.getSystem(System2.system_id).enable();

    gEM.begin();
    gEM.update();
    gEM.end();

    gEM.destroy();
}