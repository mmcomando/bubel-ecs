/************************************************************************************************************************
This module contain attributes used to mark components.
Currently only two attributes are supported:
$(LIST
    * optional: mark component as optional for system update
    * readonly: mark component access as read only (used for multithreading)
)

By default components are required and mutable. "const" attribute can be used insteac od readonly mark.

---
Struct EntitiesData
{
   Comp1[] cmp; //mutable required component
   @readonly @optional Comp2[] cmp2; //optional read only component
   @optional const (Comp3)[] cmp3; //same as cmp2
}
---

Copyright: Copyright © 2018-2019, Dawid Masiukiewicz, Michał Masiukiewicz
License: BSD 3-clause, see LICENSE file in project root folder.
*/
module bubel.ecs.attributes;

///Used to mark optional components for system.
enum optional = "optional";
///Used to mark readonly components for system. "const" can be used insted.
enum readonly = "readonly";
