/************************************************************************************************************************
This module contain main templates for user.
There are three structure templates (mixins) which should be added on top of structure:
$(LIST
	* System: make system structure
	* Component: make component structure
	* Event: make event structure
)

---
Struct System1
{
	mixin!ECS.System;
}

Struct System2
{
	mixin!ECS.System(16);//set number of jobs generated for system by multithreaded update
}

Struct Component1
{
	mixin!ECS.Component;
}

Struct Event1
{
	mixin!ECS.Event;
}
---

There is also template for generating list of excluded components "ExcludedComponets(T...)".
This template takes component structure types and making list of excluded components used in "registerSystem" function.

---
Struct System1
{
	mixin!ECS.System;

	struct EntitiesData
	{
		... //used components
	}

	ExcludedComponets!(Comp1, Comp2);
}
---

Copyright: Copyright © 2018-2019, Dawid Masiukiewicz, Michał Masiukiewicz
License: BSD 3-clause, see LICENSE file in project root folder.
*/
module bubel.ecs.core;

public import bubel.ecs.manager;
public import bubel.ecs.entity;

/************************************************************************************************************************
Main struct used as namespace for templates.
*/
static struct ECS
{
	/************************************************************************************************************************
	Mark structure as System. Should be added on top of structure (before any data).
	*/
	mixin template System(uint jobs_count = 32)
	{
		__gshared ushort system_id = ushort.max;
		uint __ecs_jobs_count = jobs_count;
	}

	/************************************************************************************************************************
	Mark structure as Component. Should be added on top of structure (before any data).
	*/
	mixin template Component()
	{
		__gshared ushort component_id = ushort.max;

		ComponentRef ref_() @nogc nothrow return 
		{
			return ComponentRef(&this, component_id);
		}
	}

	/************************************************************************************************************************
	Mark structure as Event. Should be added on top of structure (before any data).
	*/
	mixin template Event()
	{
		__gshared ushort event_id = ushort.max;
	}

	/************************************************************************************************************************
	Make list of excluded components. This template get structure types as argument. Should be added inside System structure.
	*/
	mixin template ExcludedComponents(T...)
	{
		alias ExcludedComponents = T;
	}

	/************************************************************************************************************************
	Make list of readonly ependencies. This template get strings as arguments. Should be added inside System structure.
	*/
	mixin template ReadOnlyDependencies(T...)
	{
		alias ReadOnlyDependencies = T;
	}

	/************************************************************************************************************************
	Make list of writable ependencies. This template get strings as arguments. Should be added inside System structure.
	*/
	mixin template WritableDependencies(T...)
	{
		alias WritableDependencies = T;
	}
}
