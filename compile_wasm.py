import os
import ntpath
import sys

def compile(sources, output):
    files = []
    # r=root, d=directories, f = files
    for path in sources:
        for r, d, f in os.walk(path):
            for file in f:
                if ntpath.basename(file) != 'win_dll.d':
                    filename, file_extension = os.path.splitext(file)
                    if file_extension == '.d' and filename != 'package':
                        files.append(os.path.join(r, file))

    ldc_cmd = 'ldc2 ' + shared_flags + ldc_flags + '-oq -mtriple=wasm32-unknown-unknown-wasm -betterC --output-bc --od=.bc --singleobj --checkaction=C --of=' + output + ' '

    for path in sources:
        ldc_cmd += '-I' + path + ' '
        
    for path in import_paths:
        ldc_cmd += '-I' + path + ' '
        
    for f in files:
        ldc_cmd += f + ' '

    print(ldc_cmd)

    if os.system(ldc_cmd):
        exit(0)
    print()

shared_flags = ''
clean = 0
emc_flags = ''
ldc_flags = '--d-version=ECSEmscripten '
import_paths = ['source','tests']
build_tests = 0

for arg in sys.argv[1:]:
    if(arg == '-release'):
        ldc_flags += '-release '
    elif(arg == '-enable-inlining'):
        ldc_flags += '-enable-inlining '
    elif(arg == '-O3'):
        shared_flags += '-O3 '
    elif(arg == '-O2'):
        shared_flags += '-O2 '
    elif(arg == '-O1'):
        shared_flags += '-O1 '
    elif(arg == '-O0'):
        shared_flags += '-O0 '
    elif(arg == '-Os'):
        shared_flags += '-Os '
    elif(arg == '-Oz'):
        shared_flags += '-Oz '
    elif(arg == '-g'):
        shared_flags += '-g '
    elif(arg == '-g4'):
        ldc_flags += '-g '
        emc_flags += '-g4 '
    elif(arg == '--build-tests'):
        build_tests = 1
    elif(arg == '--llvm-lto'):
        emc_flags += '--llvm-lto 3 '
    elif(arg == '--simd'):
        emc_flags += '-s SIMD=1 '
    elif(arg == '-opt'):
        shared_flags += '-O3 '
        ldc_flags += '-release -enable-inlining '
        emc_flags += '--llvm-lto 3 -s SIMD=1 '
    elif(arg == '-pthread'):
        emc_flags += '-s PTHREAD_POOL_SIZE=16 -s USE_PTHREADS=1 '
    else:
        print('unknown argument: ' + arg)
        exit()
    
compile(['source'], 'ecs.bc')
    
if build_tests == 0:
    exit(0)
    
compile(['tests'], 'tests.bc')
                
emcc_cmd = 'emcc -v ' + shared_flags + emc_flags + '-s ALLOW_MEMORY_GROWTH=1 -s WASM_MEM_MAX=1024MB  -s MALLOC=dlmalloc -s WASM=1 -o index.html '
#-s ALLOW_MEMORY_GROWTH=1

emcc_cmd += 'ecs.bc tests.bc'
#emcc_cmd += 'tests.bc'

print(emcc_cmd)

os.system(emcc_cmd)
